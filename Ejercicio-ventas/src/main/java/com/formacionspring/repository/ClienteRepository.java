package com.formacionspring.repository;

import org.springframework.data.repository.CrudRepository;

import com.formacionspring.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{

}
