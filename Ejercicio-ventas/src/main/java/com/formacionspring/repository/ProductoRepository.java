package com.formacionspring.repository;

import org.springframework.data.repository.CrudRepository;

import com.formacionspring.entity.Producto;



public interface ProductoRepository extends CrudRepository<Producto, Long>{

}
