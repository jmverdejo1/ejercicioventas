package com.formacionspring.repository;

import org.springframework.data.repository.CrudRepository;

import com.formacionspring.entity.Venta;



public interface VentaRepository extends CrudRepository<Venta, Long>{

}
