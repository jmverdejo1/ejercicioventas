package com.formacionspring.controller;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.formacionspring.entity.Cliente;
import com.formacionspring.service.ClienteService;

@RestController
@RequestMapping("api")
public class ClienteController {

	@Autowired
	private ClienteService servicio;
	
	@GetMapping("clientes")
	public List<Cliente> get(){
		return servicio.mostrarClientes();
	}
	
	
	@GetMapping("clientes/{id}")
	public ResponseEntity<?>getOne(@PathVariable long id){
		Cliente cliente = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			cliente = servicio.buscarCliente(id);
			
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (cliente == null) {
			response.put("mensaje", "El cliente con ID: "+id+" no existe en la base de datos");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Cliente>(cliente,HttpStatus.OK);
	}
	
	
	@PostMapping("clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> save(@RequestBody Cliente cliente){
		Cliente clienteNuevo = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			clienteNuevo = servicio.guardarCliente(cliente);
			
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar inserción a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "EL cliente ha sido creado con éxito");
		response.put("cliente", clienteNuevo);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@PutMapping("clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@PathVariable long id, @RequestBody Cliente cliente){
		Cliente clienteUpdate = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			clienteUpdate = servicio.buscarCliente(id);
			if (clienteUpdate != null) {
				clienteUpdate.setNombre(cliente.getNombre());
				clienteUpdate.setApellidos(cliente.getApellidos());
				clienteUpdate.setSexo(cliente.getSexo());
				clienteUpdate.setTelefono(cliente.getTelefono());
				
			} else {
				response.put("mensaje","Error: no se puede editar el cliente con ID: "+id+", no existe en la base de datos.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}
			
			servicio.guardarCliente(clienteUpdate);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar modificación a la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido actualizado con éxito");
		response.put("cliente", clienteUpdate);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED	);
	}
	
	@DeleteMapping("clientes/{id}")
	public ResponseEntity<?> delete(@PathVariable long id) {
		Cliente borrado = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			borrado = servicio.buscarCliente(id);
			if (borrado != null) {
				servicio.borrarCliente(id);
			} else {
				response.put("mensaje","Error: no se puede borrar el cliente con ID: "+id+", no existe en la base de datos.");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
			}
			
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar borrado en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido borrado con éxito");
		response.put("cliente", borrado);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
}
