package com.formacionspring.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.formacionspring.entity.Venta;
import com.formacionspring.service.VentaService;

import ch.qos.logback.core.net.server.Client;

@RestController
@RequestMapping("api")
public class VentaController {
	
	@Autowired
	private VentaService servicio;
	
	@GetMapping("ventas")
	public List<Venta> index(){
		return servicio.mostrarVentas();	
	}
	
	@GetMapping("ventas/{id}")
	public ResponseEntity<?> showVenta(@PathVariable long id){
		Venta venta = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			venta = servicio.buscarVenta(id);
			if(venta == null) {
				response.put("Mensaje", "La venta con ID: " + id + " no existe en la base de datos");
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
			}
		} catch (DataAccessException e) {
			//si hay error desde la base de datos
			response.put("Mensaje", "Error al realizar consulta en la base de dato");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Venta>(venta,HttpStatus.OK);
	}
	
	@PostMapping("ventas")
	public ResponseEntity<?> save(@RequestBody Venta venta){
		Venta ventaGuardada = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			ventaGuardada = servicio.guardarVenta(venta);
		
		} catch (DataAccessException e) {
			//si hay error desde la base de datos
			response.put("Mensaje", "Error al realizar consulta en la base de dato");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","La venta ha sido creado con exito");
		response.put("venta", ventaGuardada);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	@PutMapping("ventas/{id}")
	public ResponseEntity<?> update(@PathVariable long id, @RequestBody Venta venta){
		Venta ventaUpdate = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			ventaUpdate = servicio.buscarVenta(id);
			if(ventaUpdate !=null) {
				ventaUpdate.setCantidad(venta.getCantidad());
				ventaUpdate.setCliente(venta.getCliente());
				ventaUpdate.setIva(venta.getIva());
				ventaUpdate.setProducto(venta.getProducto());
				ventaUpdate.setSubtotal(venta.getSubtotal());
				ventaUpdate.setTotal(venta.getTotal());
			}else {
				response.put("mensaje","Error: no se puede editar,la venta con ID: " + id + " no existe en la base de datos");	
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
			}
			servicio.guardarVenta(ventaUpdate);
			
		} catch (DataAccessException e) {
			//si hay error desde la base de datos
			response.put("Mensaje", "Error al realizar consulta en la base de dato");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","La venta ha sido actualizada con éxito!");
		response.put("venta",ventaUpdate);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@DeleteMapping("ventas/{id}")
	public ResponseEntity<?> delete (@PathVariable long id){
		Venta ventaDelete = null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			ventaDelete = servicio.borrarVenta(id);
		
		} catch (DataAccessException e) {
			//si hay error desde la base de datos
			response.put("Mensaje", "Error al realizar consulta en la base de dato");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","La venta ha sido eliminada con éxito!");
        return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
}
