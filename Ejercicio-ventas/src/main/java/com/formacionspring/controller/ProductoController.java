package com.formacionspring.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.apirest.entity.Cliente;
import com.formacionspring.entity.Producto;
import com.formacionspring.service.ProductoService;

@RestController
@RequestMapping("api")
public class ProductoController {
	@Autowired
	private ProductoService servicio;
	
	@GetMapping("productos")
	public List<Producto> index(){
		return servicio.mostrarProductos();
	}
	
	@GetMapping("productos/{id}")
	public ResponseEntity<?> show(@PathVariable long id) {
		Producto producto = null;
		Map<String, Object> response = new HashMap<>();
		try {
			producto = servicio.buscarProducto(id);
		}catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("mensaje", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(producto == null) {
			response.put("mensaje", "El producto con ID: "+id+" no existe en la base de datoss");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}
	
	
	@PutMapping("productos/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@PathVariable long id,@RequestBody Producto producto) {
		Producto productoUpdate = null;
		Map<String,Object> response = new HashMap<>();
		productoUpdate = servicio.buscarProducto(id);
		
		if(productoUpdate != null) {
			productoUpdate.setNombre(producto.getNombre());
			productoUpdate.setDescripcion(producto.getDescripcion());
			productoUpdate.setExistencias(producto.getExistencias());
			productoUpdate.setPrecio(producto.getPrecio());
		}else {
			response.put("mensaje", "Error: no se puede editar, el producto con ID: "+" no existe en la base de datos.");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		try {
			servicio.guardarProducto(productoUpdate);
		}catch(DataAccessException e) {
			//si hay error desde la base de datos
			response.put("mensaje", "Error al realizar update en la base de datos");
			response.put("mensaje", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Exito al actualizar en la base de datos");
		response.put("producto", productoUpdate);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PostMapping("productos")
	public ResponseEntity<?> save(@RequestBody Producto producto) {
		Producto productosNew = null;
		Map<String,Object> response =new HashMap<String,Object>();
		
		try {
			productosNew = servicio.guardarProducto(producto);
		} catch (DataAccessException e) {
			//si hay error desde la base de datos
			response.put("mensaje","Error al realizar insert en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","El producto ha sido creado con éxito!");
		response.put("producto",productosNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	@DeleteMapping("productos/{id}")

	public ResponseEntity<?> delete(@PathVariable long id) {
		Map<String,Object> response =new HashMap<>();
		Producto productoDelete = null;
		
		try {
			
			productoDelete = servicio.borrarProducto(id);
			
		} catch (DataAccessException e) {
			//si hay error desde la base de datos
			response.put("mensaje","Error al realizar delete en la base de datos");
			response.put("error",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","El producto ha sido eliminado con éxito!");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
		
	}
}
