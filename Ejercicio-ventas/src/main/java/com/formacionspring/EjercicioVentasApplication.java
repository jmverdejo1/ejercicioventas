package com.formacionspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title="APIREST VENTAS",version="1.0",description="Crud completo de ventas"))
public class EjercicioVentasApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioVentasApplication.class, args);
	}

}
