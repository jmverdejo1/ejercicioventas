package com.formacionspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionspring.entity.Venta;
import com.formacionspring.repository.VentaRepository;

@Service
public class VentaServiceImpl implements VentaService {
	
	@Autowired
	private VentaRepository repository;

	@Override
	@Transactional(readOnly = true)
	public List<Venta> mostrarVentas() {
		
		return (List<Venta>)repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Venta buscarVenta(long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Venta guardarVenta(Venta venta) {
		return repository.save(venta);
	}

	@Override
	public Venta borrarVenta(long id) {
		Venta ventaBorrada = buscarVenta(id);
		repository.deleteById(id);
		return ventaBorrada;
	}

}
