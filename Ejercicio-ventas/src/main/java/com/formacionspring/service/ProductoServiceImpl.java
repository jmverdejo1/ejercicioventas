package com.formacionspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionspring.entity.Producto;
import com.formacionspring.repository.ProductoRepository;

@Service
public class ProductoServiceImpl implements ProductoService{
	
	@Autowired
	private ProductoRepository repository;

	@Override
	@Transactional(readOnly = true)
	public List<Producto> mostrarProductos() {
		return (List<Producto>)repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Producto buscarProducto(long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Producto guardarProducto(Producto producto) {
		return repository.save(producto);
	}

	@Override
	public Producto borrarProducto(long id) {
		Producto productoBorrado = buscarProducto(id);
		repository.deleteById(id);
		
		return productoBorrado;
		

	}


}
