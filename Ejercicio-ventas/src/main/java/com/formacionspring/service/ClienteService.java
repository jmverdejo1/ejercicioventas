package com.formacionspring.service;

import java.util.List;

import com.formacionspring.entity.Cliente;


public interface ClienteService {
	
	
	//Metodo para mostrar todos los clientes
	public List<Cliente> mostrarClientes();
			
	//metodo que busque cliente por id
	public Cliente buscarCliente(long id);
			
	//metodo para crear nuevo cliente	
	public Cliente guardarCliente(Cliente cliente);
			
	//metodo para borrar un cliente
	public void borrarCliente(long id);

}
