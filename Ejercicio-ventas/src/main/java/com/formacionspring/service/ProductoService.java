package com.formacionspring.service;

import java.util.List;

import com.formacionspring.entity.Producto;

public interface ProductoService {
	
	//Metodo para mostrar todos los productos
	public List<Producto> mostrarProductos();
		
	//metodo que busque productos por id
	public Producto buscarProducto(long id);
		
	//metodo para crear nuevo producto	
	public Producto guardarProducto(Producto producto);
		
	//metodo para borrar una producto
	public Producto borrarProducto(long id);
	

}
