package com.formacionspring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionspring.entity.Cliente;
import com.formacionspring.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	private ClienteRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Cliente> mostrarClientes(){
		
		return (List<Cliente>) repository.findAll();
		
	}

	
	@Override
	@Transactional(readOnly = true)
	public Cliente buscarCliente(long id) {
		// TODO Auto-generated method stub
		
		return repository.findById(id).orElse(null);
	}
	

	@Override
	@Transactional
	public Cliente guardarCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		return repository.save(cliente);
	}

	@Override
	@Transactional
	public void borrarCliente(long id) {
		// TODO Auto-generated method stub
		repository.deleteById(id);
	}

}
