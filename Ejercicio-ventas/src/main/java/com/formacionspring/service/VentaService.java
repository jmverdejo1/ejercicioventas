package com.formacionspring.service;

import java.util.List;

import com.formacionspring.entity.Venta;

public interface VentaService {
	
	//Metodo para mostrar todos las ventas
	public List<Venta> mostrarVentas();
		
	//metodo que busque cliente por folio
	public Venta buscarVenta(long id);
		
	//metodo para crear nueva venta	
	public Venta guardarVenta(Venta venta);
		
	//metodo para borrar una venta
	public Venta borrarVenta(long id);
}
